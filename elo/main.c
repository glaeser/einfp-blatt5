#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct player{
	//TODO
}player;

float predict(player* A, player* B){
	//TODO
}

void update(player* A, player* B, double result){
	//TODO
}

int main(void) {
  	player A = {"Magnus Carlsen",2855, 25};
  	player B = {"Jan Nepomnjaschtschi", 2782, 28};
	update(&A,&B,0.5);  //1
	update(&A,&B,0.5);  //2
	update(&A,&B,0.5);  //3
	update(&A,&B,0.5);  //4
	update(&A,&B,0.5);  //5
	update(&A,&B,1);  //6
	update(&A,&B,0.5);  //7
	update(&A,&B,1);  //8
	update(&A,&B,1);  //9
	update(&A,&B,0.5);  //10
	update(&B,&A,0);  //11
	// In reality players with consistent rating above 2400 use a k-value of 10.
	printf("rating after 11 games:\n");
	printf("%s: %i \n", A.name, (int)A.rating);
	printf("%s: %i \n", B.name, (int)B.rating);
}
