#include <stdio.h>
#include <stdlib.h>
#include "set.h"
#include "unity.h"


void setUp(void)
{
}

void tearDown(void)
{
}

void test_insert_empty(){
	node * head = NULL;
	set_insert(&head,1);
	TEST_ASSERT_EQUAL_INT(1, head->data);
}

void test_insert_at_start(){
	node * head = NULL;
	set_insert(&head,1);
	set_insert(&head,-1);
	TEST_ASSERT_EQUAL_INT(-1, head->data);
	TEST_ASSERT_EQUAL_INT(1, head->next->data);
	TEST_ASSERT_EQUAL_INT(2,set_count(head));
}

void test_insert_existing(){
	node * head = NULL;
	set_insert(&head,1);
	set_insert(&head,1);
	TEST_ASSERT_EQUAL_INT(1,set_count(head));
}

void test_insert(){
	node * head = NULL;
	for (int i = 1; i < 20; i =i+3) {
		set_insert(&head,i);
	}
	set_insert(&head,-1);
	set_insert(&head,1);
	set_insert(&head,30);
	TEST_ASSERT_EQUAL_INT(9,set_count(head));
}

void test_delete(){
	node * head = NULL;
	for (int i = 1; i < 20; i =i+3) {
		set_insert(&head,i);
	}
	set_delete(&head,-1);
	set_delete(&head,1);
	set_delete(&head,10);
	set_delete(&head,10);
	set_insert(&head,10);
	set_delete(&head,19);
	set_delete(&head,25);
	TEST_ASSERT_EQUAL_INT(5,set_count(head));
}

// Damit test_merge() ausgeführt wird, muss es ebenfalls in test_runners/TestSet_Runner.c einkommentiert werden
// void test_merge(){
// 	node * head1 = NULL;
// 	node * head2 = NULL;
// 	for (int i = 1; i < 20; i =i+3) {
// 		set_insert(&head1,i);
// 	}
// 	set_insert(&head2,-1);
// 	set_insert(&head2,1);
// 	set_insert(&head2,30);
// 	node * newhead = set_union(head1,head2);
// 	TEST_ASSERT_EQUAL_INT(9,set_count(newhead));
// }

