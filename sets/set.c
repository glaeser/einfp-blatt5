#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "set.h"

//typedef struct node{
//	int data;
//	struct node * next;
//}node;

node * new_node(int data) {
	node * e = (node *)malloc(sizeof(node));
	e->data =data;
	e->next = NULL;
	return e;
}

void free_node(node * node){
	free(node);
}

void set_insert(node** head, int data){
	//TODO
}

void set_delete (node ** head, int data){
	//TODO
}

void set_print(node * head){
	node * cur = head;
	while (cur != NULL) {
		printf("%i\n", cur->data);
		cur = cur->next;
	}
}

int set_count(node * head){
	node * cur = head;
	int count = 0;
	while (cur != NULL) {
		count++;
		cur = cur->next;
	}
	return count;
}
