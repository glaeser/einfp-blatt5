#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "set.h"

int main(void) {
	node * head = NULL;
	for (int i = 1; i < 40; i =i+3) {
		set_insert(&head,i);
	}
	for (int i = 2; i < 20; i =i+2) {
		set_delete(&head,i);
	}
	set_delete(&head,-1);
	set_delete(&head,297);
	set_insert(&head, -100);
	set_print(head);

	head = NULL;
	for (int i = 1; i < 20; i =i+3) {
		set_insert(&head,i);
	}
	set_insert(&head,-1);
	set_insert(&head,1);
	set_insert(&head,30);
	set_print(head);

}
